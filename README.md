# Reservo-test

Esta es una aplicación Django REST que proporciona una API para realizar operaciones CRUD (Crear, Leer, Actualizar y Eliminar) en un modelo de datos.

## Requisitos previos

Asegúrate de tener instalado lo siguiente en tu entorno de desarrollo:

- Python (versión 3.7)
- Pip (un administrador de paquetes para Python)

## Instalación

Sigue estos pasos para configurar y ejecutar la aplicación:

1. Clona el repositorio de la aplicación.

```bash
git clone https://github.com/tu_usuario/tu_aplicacion.git
```

2. Navega hasta el directorio de la aplicación.

```bash
cd reservo-test
```

3. Crea y activa un entorno virtual.

```bash
python -m venv venv
source venv/bin/activate
```

4. Instala las dependencias del proyecto.

```bash
pip install -r requirements.txt
```

5. Realiza las migraciones de la base de datos.

```bash
python manage.py migrate
```

6. Inicia el servidor de desarrollo.

```bash
python manage.py runserver
```

7. Accede a la aplicación en tu navegador web ingresando la siguiente URL:

```
http://localhost:8000/
```

## Uso

Una vez que el servidor esté en funcionamiento, puedes interactuar con la API a través de los siguientes puntos finales:

- `/pets/`: Obtiene una lista de todos las mascotas ingresadas al sistema.
- `/pets/<id>/`: Obtiene, actualiza o elimina una mascota en específico.
- `/patients/`: Obtiene una lista de todos los pacientes ingresados al sistema.
- `/patients/<id>/`: Obtiene, actualiza o elimina un paciente en específico.

- `/patients-list/` (GET method): Obtiene una lista de todos los pacientes obtenidos desde el endpoint https://3y1hl3jca0.execute-api.us-east-1.amazonaws.com/pacientes_endpoint.
- `/patients-list/` (POST method): Actualiza la base de datos del sistema a partir de los pacientes del endpoint https://3y1hl3jca0.execute-api.us-east-1.amazonaws.com/pacientes_endpoint.

Puedes utilizar herramientas como cURL, Postman o tu navegador web para realizar solicitudes HTTP a la API.
