from django.db import models
from patients.models import Patient

GENDERS = [(0, 'Female'), (1, 'Male')]


class Pet(models.Model):
  name = models.CharField(max_length=100, blank=True, default='')
  breed = models.TextField(null=True, blank=True)
  gender = models.CharField(choices=GENDERS, max_length=10)
  country = models.TextField(null=True, blank=True)
  color = models.TextField(null=True, blank=True)
  patient = models.ForeignKey(Patient, on_delete=models.CASCADE, blank=True, null=True)

  created = models.DateTimeField(auto_now_add=True)
  updated = models.DateTimeField(auto_now=True)

