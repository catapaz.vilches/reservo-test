from rest_framework import viewsets
from rest_framework.decorators import api_view
from .models import Patient
from .serializers import PatientSerializer
from rest_framework.response import Response
import requests

@api_view(['GET', 'POST'])
def patient_list(request, format=None):
    """
    List all code patients, or create a new patient.
    """
    if request.method == 'GET':
      print("request: ", request)
      response = requests.get('https://3y1hl3jca0.execute-api.us-east-1.amazonaws.com/pacientes_endpoint')

      return Response(response.json())
    
    if request.method == 'POST':
      response = requests.get('https://3y1hl3jca0.execute-api.us-east-1.amazonaws.com/pacientes_endpoint')
      results = response.json()["results"]

      for object in results:
        uuid = object["uuid"]
        name = object["nombre"]
        mother_lastname = object["apellido_materno"]
        father_lastname = object["apellido_paterno"]
        my_data = Patient.objects.filter(uuid=uuid)
        if len(my_data) == 0:
          patient = Patient(uuid=uuid, name=name, mother_lastname=mother_lastname, father_lastname=father_lastname)
          patient.save()
        else:
          for element in my_data:
            element.delete()
          patient = Patient(uuid=uuid, name=name, mother_lastname=mother_lastname, father_lastname=father_lastname)
          patient.save()

      return Response(response.json())
         

class PatientViewSet(viewsets.ModelViewSet):
  """
  This viewset automatically provides `list`, `create`, `retrieve`,
  `update` and `destroy` actions.

  """
  queryset = Patient.objects.all()
  serializer_class = PatientSerializer

