from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register(r'patients', views.PatientViewSet, basename="patient")

urlpatterns = [
    path('', include(router.urls)),
    path('patients-list/', views.patient_list)
]