from django.db import models

class Patient(models.Model):
  uuid = models.CharField(max_length=100)
  name = models.CharField(max_length=100, blank=True, default='')
  mother_lastname = models.CharField(max_length=100, blank=True, default='')
  father_lastname = models.CharField(max_length=100, blank=True, default='')

  created = models.DateTimeField(auto_now_add=True)
  updated = models.DateTimeField(auto_now=True)
