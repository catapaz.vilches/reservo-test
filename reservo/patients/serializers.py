from rest_framework import serializers
from .models import Patient


class PatientSerializer(serializers.ModelSerializer):
  
  class Meta:
    model = Patient
    fields = ['id', 'uuid', 'name', 'mother_lastname', 'father_lastname']

# class PatientSerializer(serializers.Serializer):
#   id = serializers.CharField(read_only=True)
#   name = serializers.CharField(max_length=100)
#   mother_lastname = serializers.CharField(required=False, allow_blank=True, max_length=100)
#   father_lastname  = serializers.CharField(required=False, allow_blank=True, max_length=100)

# def create(self, validated_data):
#   """
#   Create and return a new `Patient` instance, given the validated data.
#   """
#   return Patient.objects.create(**validated_data)

# def update(self, instance, validated_data):
#   """
#   Update and return an existing `Patient` instance, given the validated data.
#   """
#   instance.id = validated_data.get('uuid', instance.id)
#   instance.name = validated_data.get('nombre', instance.name)
#   instance.mother_lastname = validated_data.get('apellido_materno', instance.mother_lastname)
#   instance.father_lastname = validated_data.get('apellido_paterno', instance.father_lastname)
#   instance.save()
#   return instance